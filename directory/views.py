from django.shortcuts import render, get_object_or_404
from .models import Manufacturer

# from django.http import HttpResponse


# Create your views here.

def index(request):
    manufacturers = Manufacturer.objects.all()
    return render(request,'manufacturers.html', {'manufacturers': manufacturers})