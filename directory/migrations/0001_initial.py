# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('slogan', models.CharField(default='no one', max_length=500)),
                ('email', models.CharField(max_length=100)),
                ('telephone', models.CharField(max_length=20)),
                ('image', models.CharField(default='none', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=100)),
                ('upvotes', models.IntegerField(default=0)),
                ('downvotes', models.IntegerField(default=0)),
                ('bio', models.TextField()),
                ('votes', models.IntegerField(default=0)),
                ('facebook', models.CharField(default='none', max_length=200)),
                ('twitter', models.CharField(default='none', max_length=200)),
                ('instagram', models.CharField(default='none', max_length=200)),
                ('linkedin', models.CharField(default='none', max_length=200)),
                ('manufacturer', models.ForeignKey(to='directory.Manufacturer')),
            ],
        ),
    ]
