from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Manufacturer(models.Model):
    name = models.CharField(max_length=100)
    slogan = models.CharField(max_length=500, default='no one')
    email = models.CharField(max_length=100)
    telephone = models.CharField(max_length=20)
    image = models.CharField(max_length=200, default='none')

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Product(models.Model):
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=100)
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)
    bio = models.TextField()
    votes = models.IntegerField(default=0)
    facebook = models.CharField(max_length=200, default='none')
    twitter = models.CharField(max_length=200, default='none')
    instagram = models.CharField(max_length=200, default='none')
    linkedin = models.CharField(max_length=200, default='none')

    def containsLinkedin(self):
        return self.linkedin == 'none'

    def containsTwitter(self):
        return self.twitter == 'none'

    def conatinsFacebook(self):
        return self.facebook == 'none'

    def containsInstagram(self):
        return self.instagram == 'none'

    def __str__(selfs):
        return self.product_name