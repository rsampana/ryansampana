$project_user = "addressbook"
$project_name = "addressbook"
$db_pass = "8a51e3872d774579b1e2683db034cd26"
$git_repo = "git@bitbucket.org:nixateam/project-starter.git"  # tODO git address here

class { "deploy": } ->

class { "django":
  prod         => true,
} ->

django::project{ $project_name:
  project_name => $project_name,
  project_user => $project_user,
  project_path => "/home/$project_user/project",
  db_user      => $project_user,
  db_name      => $project_name,
  db_pass      => $db_pass,
  git_host     => "bitbucket.org",
  git_repo     => $git_repo,
  prod         => true,
  staging      => true,
  branch       => $::branch,
}
