Nixa Gulp

Default Tasks
------------

    gulp - Watches SASS folder and runs styles task.
    gulp styles - Compiles the styles.
    gulp vendors - Compiles vendor fonts, js and css.