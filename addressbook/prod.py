from os import path

DB_USER = 'addressbook'
DB_NAME = 'addressbook'
DB_PASS = '8a51e3872d774579b1e2683db034cd26'
ALLOWED_HOSTS = None,  # tODO add prod IP here.
PROJECT_PROTOCOL = 'http://'
PROJECT_DOMAIN = 'todo' # TODO add prod ip here.
PROJECT_URI = "".join((PROJECT_PROTOCOL, PROJECT_DOMAIN))

DEBUG = False
PROJECT_NAME = path.basename(path.dirname(__file__))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': DB_USER,
        'NAME': DB_NAME,
        'PASSWORD': DB_PASS,
        'HOST': 'localhost',
    },
}

CACHES = {
    "default": {
        'BACKEND': 'redis_cache.RedisCache',
        "LOCATION": "redis://localhost:6379/0",
        'TIMEOUT': 300,
        'KEY_PREFIX': 'django-%s-' % PROJECT_NAME,
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

TEMPLATES = (
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ('templates/', ),
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': (
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ),
            'loaders': (
                ('django.template.loaders.cached.Loader', (
                    'django.template.loaders.app_directories.Loader',
                    'django.template.loaders.filesystem.Loader',
                )),
            ),
        },
    },
)
